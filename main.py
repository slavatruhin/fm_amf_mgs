import os
import numba
import numpy
import matplotlib.pyplot as plt
import configparser

conf = configparser.ConfigParser()
conf.read("data/conf.ini")
dos_dir = "data/dos" + conf["Sample"]["spins"] + "/"
stat_dir = "data/statsum" + conf["Sample"]["spins"] + "/"
fig_dir = "data/figures" + conf["Sample"]["spins"] + "/"
T = numpy.linspace(float(conf["Temperature"]["start"]),
                   float(conf["Temperature"]["stop"]),
                   int(conf["Temperature"]["steps"]))
H = numpy.linspace(float(conf["Field"]["start"]),
                   float(conf["Field"]["stop"]),
                   int(conf["Field"]["steps"]))
# H = numpy.array([0, 0.4, 1])


@numba.njit(parallel=True)
def fm_probability_Ggs(g, e, m, e_total, mass_t, n, J):
    ferro_probability = numpy.zeros(mass_t.size)
    ferro_probability_t0 = numpy.zeros(mass_t.size)
    for t in numba.prange(mass_t.size):
        ferro_probability[t] = calc_fm_probability_Ggs(g, m, e_total, mass_t[t], n)
        ferro_probability_t0[t] = calc_fm_amf_probability_Ggs_no_T(g, e, m, n, J)
    return ferro_probability


@numba.njit
def calc_fm_probability_Ggs(g, m, e_total, t, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    prob = stat / z
    energy = e_total * prob
    e_min = min(energy)
    g_gs = 0
    m_gs_fm = 0
    for i in range(len(energy)):
        if energy[i] <= e_min:
            g_gs += g[i]
            if abs(m_gs_fm) <= abs(m[i]):
                m_gs_fm = m[i]
    if g_gs > 0:
        # return (2 / g_gs) * (abs(m_gs_fm)) / (n * n)
        return (abs(m_gs_fm)) / (n * n)
    else:
        return 0


@numba.njit
def calc_fm_amf_probability_Ggs_no_T(g, e, m, n, J):
    e_gs = min(e)
    g_gs = 0
    for i in range(len(e)):
        if e[i] <= e_gs:
            gs = i
            g_gs += g[i]
    if g_gs > 0:
        return (2 / g_gs) * ((n * n - abs(m[gs])) / (n * n))
    else:
        return 0


@numba.njit(parallel=True)
def afm_probability_Ggs(g, e, m, e_total, mass_t, n, J):
    antiferro_probability = numpy.zeros(mass_t.size)
    for t in numba.prange(mass_t.size):
        antiferro_probability[t] = calc_afm_probability_Ggs(g, m, e_total, mass_t[t], n)
    return antiferro_probability


@numba.njit
def calc_afm_probability_Ggs(g, m, e_total, t, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    prob = stat / z
    energy = e_total * prob
    e_min = min(energy)
    g_gs = 0
    m_gs_fm = 0
    m_gs_afm = n * n
    for i in range(len(energy)):
        if energy[i] <= e_min:
            g_gs += g[i]
            if abs(m_gs_afm) >= abs(m[i]):
                m_gs_afm = m[i]
            if abs(m_gs_fm) <= abs(m[i]):
                m_gs_fm = m[i]
    if g_gs > 0:
        return (2 / g_gs) * ((n * n - abs(m_gs_afm)) / (n * n))
    else:
        return 0


if conf["Recalculate"]["recalculate"] == "True":
    for h in H:
        P_plus = numpy.array([])
        p_fm = numpy.zeros(shape=(0, int(conf["Temperature"]["steps"])))
        p_afm = numpy.zeros(shape=(0, int(conf["Temperature"]["steps"])))
        for file in os.listdir(dos_dir):
            file = dos_dir + file
            gem = numpy.loadtxt(open(file), skiprows=4).T
            N = int(open(file).readlines()[0].rstrip())
            J_sum = int(open(file).readlines()[2].rstrip())
            P_plus = numpy.append(P_plus, (2 * N * (N - 1) + J_sum) / (4 * N * (N - 1)))
            G = gem[0]
            E = gem[1]
            M = gem[2]
            E_total = E - M * h
            p_fm = numpy.append(p_fm, [fm_probability_Ggs(G, E, M, E_total, T, N, J_sum)], axis=0)
            p_afm = numpy.append(p_afm, [afm_probability_Ggs(G, E, M, E_total, T, N, J_sum)], axis=0)
        numpy.save(stat_dir + "P_+_h" + str(h), P_plus)
        numpy.savetxt(stat_dir + "P_+_h" + str(h) + ".txt", P_plus)
        numpy.save(stat_dir + "Pfm_h" + str(h), p_fm)
        numpy.savetxt(stat_dir + "Pfm_h" + str(h) + ".txt", p_fm)
        numpy.save(stat_dir + "Pafm_h" + str(h), p_afm)
        numpy.savetxt(stat_dir + "Pafm_h" + str(h) + ".txt", p_afm)
label = []
plt.figure(dpi=300)
plt.grid(True)
plt.xlim([0, 1])
# plt.ylim([0, 3.2])
h = int(conf["Plot"]["field"])
print("h = ", H[h])
ls = list()
t_label = 0
marklist = ['+', '*', 'o']
export_phase_dots_T = numpy.array([])
export_phase_dots_P_plus = numpy.array([])
for temp in range(len(T)):
    for file in os.listdir(stat_dir):
        P_plus_loaded = numpy.load(stat_dir + "P_+_h" + str(H[h]) + ".npy")
        p_fm_loaded = numpy.load(stat_dir + "Pfm_h" + str(H[h]) + ".npy")
        p_afm_loaded = numpy.load(stat_dir + "Pafm_h" + str(H[h]) + ".npy")
        idx = P_plus_loaded.argsort()
        P_plus_loaded = P_plus_loaded[idx]
        p_fm_loaded = p_fm_loaded[idx]
    p_fm_only = p_fm_loaded[:, temp]
    p_afm_only = p_afm_loaded[:, temp]
    if conf["Plot"]["plot"] == "Mgs":
        plt.scatter(P_plus_loaded, p_fm_only, s=5)
        plt.xlabel("$P_{+}$", fontsize=18)
        plt.ylabel("$FM/AFM_{probability}$", fontsize=18)
        # label.append(f"T = {int(T[temp] * 10) / 10}")
        # plt.legend(label, fontsize=18)
        plt.tick_params(axis='both', which='major', labelsize=12)
        t_label += 1
    if conf["Plot"]["plot"] == "Phase_diagram":
        for p in range(len(P_plus_loaded)):
            if p_fm_only[p] >= 0.8:
                break
        export_phase_dots_T = numpy.append(export_phase_dots_T, T[temp])
        export_phase_dots_P_plus = numpy.append(export_phase_dots_P_plus, P_plus_loaded[p])
        plt.scatter(P_plus_loaded[p], T[temp], s=8, color='black')
        plt.xlabel("$P_{+}$", fontsize=18)
        plt.ylabel("$kT/J$", fontsize=18)
        plt.tick_params(axis='both', which='major', labelsize=12)
    if conf["Plot"]["plot"] == "Phase_diagram2":
        stop = 0
        for p in range(len(P_plus_loaded)):
            if p_afm_only[p] >= 0.8:
                stop = p
        plt.scatter(P_plus_loaded[stop], T[temp], s=8, color='black')
        plt.xlabel("$P_{+}$", fontsize=18)
        plt.ylabel("$kT/J$", fontsize=18)
        plt.tick_params(axis='both', which='major', labelsize=12)
# numpy.savetxt(stat_dir + "Phase_dots_T_h" + str(H[h]) + ".txt", export_phase_dots_T)
# numpy.savetxt(stat_dir + "Phase_dotsP_plus_h" + str(H[h]) + ".txt", export_phase_dots_P_plus)
plt.show()
